# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_24_083312) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "attendances", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.bigint "employee_id"
    t.datetime "checkin"
    t.datetime "checkout"
    t.string "selfie"
    t.decimal "checkinLat", precision: 18, scale: 15
    t.decimal "checkinLng", precision: 18, scale: 15
    t.decimal "checkoutLat", precision: 18, scale: 15
    t.decimal "checkoutLng", precision: 18, scale: 15
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["employee_id"], name: "index_attendances_on_employee_id"
  end

  create_table "employees", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "employeeCode"
    t.string "fullname"
    t.string "idCardNo"
    t.string "taxCardNo"
    t.datetime "joinAt"
    t.string "email"
    t.string "phone"
    t.datetime "bod"
    t.string "pob"
    t.string "idCardAddress"
    t.string "currentAddress"
    t.string "emergencyContactName"
    t.string "emergencyContact"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.bigint "employee_id"
    t.decimal "role"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["employee_id"], name: "index_users_on_employee_id"
  end

end
