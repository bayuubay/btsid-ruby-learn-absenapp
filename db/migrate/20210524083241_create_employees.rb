class CreateEmployees < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'pgcrypto'
    create_table :employees, id: :uuid do |t|
      t.string :employeeCode
      t.string :fullname
      t.string :idCardNo
      t.string :taxCardNo
      t.datetime :joinAt
      t.string :email
      t.string :phone
      t.datetime :bod
      t.string :pob
      t.string :idCardAddress
      t.string :currentAddress
      t.string :emergencyContactName
      t.string :emergencyContact

      t.timestamps
    end
  end
end
