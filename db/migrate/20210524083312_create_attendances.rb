class CreateAttendances < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'pgcrypto'
    create_table :attendances, id: :uuid do |t|
      t.belongs_to :employee
      t.datetime :checkin
      t.datetime :checkout
      t.string :selfie
      t.decimal :checkinLat, precision: 18, scale: 15
      t.decimal :checkinLng, precision: 18, scale: 15
      t.decimal :checkoutLat, precision: 18, scale: 15
      t.decimal :checkoutLng, precision: 18, scale: 15

      t.timestamps
    end
  end
end
