class User < ApplicationRecord
    belongs_to :employee
    has_secure_password
    validates :username, presence: true
    validates :email, presence: true
    validates :password, presence: true
    
end
