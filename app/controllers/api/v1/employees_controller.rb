module Api
    module V1
        class EmployeesController < ApplicationController
            # before_action :set_employee, only: [:show, :update, :delete]
            def index
                render json: {message:"test employee"}.to_json
                
            end

            def show
                @employee = Employee.find(params[:id])
                puts @employee
                render json: {result: true, data:@employee}
            end

            def create
                @employee = Employee.new(employee_params)

                if @employee.save
                    render json: {data:@employee,message:"success"}.to_json, status: :created
                else
                    render json: {message:"failed"}.to_json
                end
            end
            def update
                @employee = Employee.find(params[:id])
                @employee.update(employee_params)

                if @employee.valid?
                    render json: {message:"success"}.to_json, status: :ok
                else
                    render json: {message:"failed"}.to_json
                end
            end

            def destroy
                @employee = Employee.find(params[:id])
                if @employee.destroy
                    render json: {message:"success delete data employee"}.to_json, status: :ok
                else
                    render json: {message:"failed"}.to_json
                end
                
            end
            
            private
            def employee_params
                params.require(:employee).permit(:employeeCode, :fullname, :idCardNo, :taxCardNo,:joinAt,:email,:phone,:bod,:pob,:idCardAddress,:currentAddress,:emergencyContactName,:emergencyContact)
            end
            def set_employee
                current_employee = @employee              
            end
        end
    end
end